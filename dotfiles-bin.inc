whitelist ${HOME}/dotfiles/bin
whitelist ${HOME}/.bin
read-only ${HOME}/dotfiles/bin
read-only ${HOME}/.bin

env PATH=/home/ahrs/.bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
