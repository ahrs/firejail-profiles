whitelist ${HOME}/dotfiles/config/mimeapps.list
whitelist ${HOME}/.config/mimeapps.list
read-only ${HOME}/dotfiles/config/mimeapps.list
read-only ${HOME}/.config/mimeapps.list

whitelist ${HOME}/dotfiles/local/share/applications
whitelist ${HOME}/.local/share/applications
read-only ${HOME}/dotfiles/local/share/applications
read-only ${HOME}/.local/share/applications

env XDG_DATA_DIRS=/home/ahrs/dotfiles/local/share:/home/ahrs/dotfiles/local/share/flatpak/exports/share:/var/lib/flatpak/exports/share:/usr/local/share:/usr/share
